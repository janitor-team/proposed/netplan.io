#!/bin/sh
set -xe
# disable ifupdown
rm -f /etc/network/interfaces
# enable systemd-networkd
systemctl unmask systemd-networkd.service
systemctl unmask systemd-networkd.socket
systemctl unmask systemd-networkd-wait-online.service
systemctl enable --now systemd-networkd.service
# enable systemd-resolved
systemctl unmask systemd-resolved.service
systemctl enable --now systemd-resolved.service
# enable systemd-udevd
mount -o remount,rw /sys
systemctl unmask systemd-udevd.service
systemctl start systemd-udevd.service
